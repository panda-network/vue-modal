import Vue from 'vue'
import Modal from './Modal.vue'
import Social from './Social.vue'
import _ from 'lodash'
// import axios from 'axios'

var app = new Vue({
  el: '#app',
  components: {
    'modal' : Modal,
    'social' : Social,
  },
  methods: {
    openModal : function() {
        this.$refs.modal.open();
    }
  }
});

app.openModal();
